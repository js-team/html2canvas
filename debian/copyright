Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: html2canvas
Source: https://github.com/niklasvh/html2canvas
Files-Excluded: dist/* tests/mocha/lib/* tests/assets/jquery-*.js

Files: *
Copyright: 2012-2016 Niklas von Hertzen <niklasvh@gmail.com>
License: Expat

Files: debian/*
Copyright: 2016 Ximin Luo <infinity0@debian.org>
License: Expat

Files: debian/missing-sources/punycode.js
Copyright: 2014 Mathias Bynens <https://mathiasbynens.be/>
Comment:
 https://raw.githubusercontent.com/bestiejs/punycode.js/v1.4.1/punycode.js
 .
 punycode in Debian is v2.0.1 and only supports ES6, not ES5. However the
 latter is what html2canvas needs. Incidentally one figures out that punycode
 is needed at all, by downloading the pre-built html2canvas.js from npm and
 looking through the embedded JS code. They forgot to declare it as a
 dependency in package.json or anywhere else.
License: Expat

Files: debian/missing-sources/mocha-phantomjs-core.js
       debian/missing-sources/browser-shim.js
Copyright: 2013-2016 Nathan Black <nathan@nathanblack.org> (https://github.com/nathanboktae)
           2012-2013 Ken Collins <ken@metaskills.net> (http://metaskills.net/)
           2012-2016 mocha-phantomjs-core contributors (https://github.com/nathanboktae/mocha-phantomjs-core/blob/master/package.json)
Comment:
 https://github.com/nathanboktae/mocha-phantomjs-core/blob/9112813fa2665c33c1ed3140c652e96d01de7a89/mocha-phantomjs-core.js
 https://github.com/nathanboktae/mocha-phantomjs-core/blob/9112813fa2665c33c1ed3140c652e96d01de7a89/browser-shim.js
 .
 These two files are needed to run the tests.
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
