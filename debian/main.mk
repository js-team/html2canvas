#!/usr/bin/make -f

NODE_PATH = $(CURDIR):/usr/lib/nodejs
BROWSERIFY = NODE_PATH=$(NODE_PATH) browserify-lite

HEADER = \
	python -c 'import json; x = json.load(open("package.json")); \
	  print("/*\n  %s %s <%s>\n  Copyright (c) 2016 %s\n\n  Released under %s License\n*/\n" \
	     % (x["name"], x["version"], x["homepage"], x["author"]["name"], x["license"]))'

ALL = dist/html2canvas.js dist/html2canvas.min.js

all: $(ALL)

dist/html2canvas.js: src/core.js debian/html2canvas.js.header debian/html2canvas.js.footer debian/main.mk
	mkdir -p dist
	rm -f "$@" && touch "$@"
	$(HEADER) >> "$@"
	cat debian/html2canvas.js.header >> "$@"
# "browserify" in Gruntfile.js
	$(BROWSERIFY) "$<" --outfile "$@.tmp" --standalone html2canvas
# The below is basically what the "browserify-derequire" plugin does, as
# upstream seems to want it in Gruntfile.js
	sed -i \
	  -e 's/(\s*require\s*,/(_dereq_,/g' \
	  -e 's/require\(([^)]*)\)/_dereq_\1/g' \
	  "$@.tmp"
	cat "$@.tmp" >> "$@"
	cat debian/html2canvas.js.footer >> "$@"
	rm "$@.tmp"

dist/%.min.js: dist/%.js
# "uglify" in Gruntfile.js
	uglifyjs --preamble "$$($(HEADER))" "$<" -o "$@"

check:
# "mochacli" in Gruntfile.js
	find tests/node/ -name '*.js' \
	  | xargs -rn1 mocha -R spec
# "mocha_phantomjs" in Gruntfile.js
	find tests/mocha/ -path tests/mocha/lib -prune -o -name '*.html' -print \
	  | xargs -rn1 -I '{}' phantomjs mocha-phantomjs-core.js '{}' spec '{ "useColors": "true" }'
# Running the webdriver tests are more complex. We can most likely bypass the
# grunt plugins (like we do for the above) and chromedriver and firefoxdriver
# are already in Debian. However tests/selenium has extra dependencies that are
# not, notably node-wd to interact with the webdrivers.

clean:
	rm -rf $(ALL)

.PHONY: all check clean
